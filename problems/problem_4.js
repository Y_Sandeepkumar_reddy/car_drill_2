// The accounting team needs all the years from every car on the lot.
// Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


function problem_4(inventory) {


const carYears = inventory.map((car) =>car.car_year);
return carYears;


}
 

module.exports = problem_4 ;