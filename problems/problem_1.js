
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const inventory = require("../data/inventory");


function problem_1(inventory,id)
{
 
const car = inventory.find((car) => car.id===id);
    return car;

}

const result = problem_1(inventory,33)
console.log(result)





module.exports=problem_1;