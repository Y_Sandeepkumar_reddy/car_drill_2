// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars. 
// Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


function problem_6(inventory){
    const bmw_Audi = inventory.filter((car) => car.car_make === 'BMW' || car.car_make === 'Audi');
    return bmw_Audi;
}

module.exports =  problem_6 ;