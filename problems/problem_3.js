// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


function problem_3 (inventory){
const shortedModel = inventory.map((car) => car.car_model).sort();
 return  shortedModel ;

}

module.exports = problem_3 ;